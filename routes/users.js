var express = require('express');
var router = express.Router();
var md5 = require('MD5');
var async = require('async');
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');

/*
 * --------------------------------------------------------------------------
 * register_user
 * INPUT : firstName, lastName, email, password, deviceType, deviceToken, appVersion
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/register_user', function (req, res, next) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var manValues = [firstName, lastName, email, password, deviceType, deviceToken, appVersion];

    async.waterfall([
            function (callback) {
                func.checkBlank(res, manValues, callback);
            },
            function (callback) {
                checkEmailAvailability(res, email, callback);
            },
            function (callback) {
                func.checkAppVersion(res,deviceType, appVersion, callback);
            }], function (err,updatePopup, critical) {

            var loginTime = new Date();
            var accessToken = func.encrypt(email + loginTime);
            var encryptPassword = md5(password);

            var sql = "INSERT INTO `users`(`first_name`, `last_name`, `email`, `password`,`access_token`,`device_type`,`device_token`,`updated_at`,`app_version`) VALUES (?,?,?,?,?,?,?,?,?)";
            var values = [firstName, lastName, email, encryptPassword, accessToken, deviceType, deviceToken, loginTime, appVersion];

            dbConnection.Query(res, sql, values, function (userInsertResult) {

                var data = {
                    access_token: accessToken,
                    update_popup: updatePopup,
                    critical: critical,
                    first_name: firstName,
                    last_name: lastName
                };
                sendResponse.sendSuccessData(data, res);
            });
        }
    );
});

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function checkEmailAvailability(res, email, callback) {

    var sql = "SELECT `user_id` FROM `users` WHERE `email`=? limit 1";
    var values = [email];

    dbConnection.Query(res, sql, values, function (userResponse) {

        if (userResponse.length) {
            var errorMsg = 'Email already exists!';
            sendResponse.sendErrorMessage(errorMsg, res);
        }
        else {
            callback();
        }
    });
}

module.exports = router;